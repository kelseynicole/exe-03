var scene = new THREE.Scene();
var camera = new THREE.PerspectiveCamera( 25, window.innerWidth/window.innerHeight, 0.37, 1000 );
var mesh;

var renderer = new THREE.WebGLRenderer();
renderer.setSize( window.innerWidth, window.innerHeight );
document.body.appendChild( renderer.domElement );

var geometry = new THREE.SphereGeometry(6, 6, 6);
THREE.ImageUtils.crossOrigin = true;
var textureLoader = new THREE.TextureLoader();
textureLoader.crossOrigin = true;
/*installed Allow-Control-Allow-Origin chrome extension to get image texture to load in browser (not sure if 
this was the proper work around).  Also, for some reason the image texture only appears when launched from the 
index.html file and not from the Browser www in UwAmp*/ 
textureLoader.load('https://cdn.pixabay.com/photo/2013/08/28/12/03/plumage-176723_1280.jpg', function(texture) {
  texture.wrapS = texture.wrapT =   THREE.RepeatWrapping;
    texture.repeat.set( 4, 4 );
    var material = new THREE.MeshLambertMaterial( {map: texture, wireframe: true} );
  mesh = new THREE.Mesh( geometry, material );
  scene.add( mesh );
  
  render();
});


camera.position.z = 40;


// so many lights
var light = new THREE.DirectionalLight( 0xffffff, 3 );
light.position.set( 0, 1, 0 );
scene.add( light );

var light = new THREE.DirectionalLight( 0xffffff, 0.5 );
light.position.set( 0, -1, 0 );
scene.add( light );

var light = new THREE.DirectionalLight( 0xffffff, 3 );
light.position.set( 1, 0, 0 );
scene.add( light );

var light = new THREE.DirectionalLight( 0xffffff, 0.5 );
light.position.set( 0, 0, 1 );
scene.add( light );

var light = new THREE.DirectionalLight( 0xffffff, 3 );
light.position.set( 0, 0, -1 );
scene.add( light );

var light = new THREE.DirectionalLight( 0xffffff, 0.5 );
light.position.set( -1, 0, 0 );
scene.add( light );


var render = function () {
  requestAnimationFrame( render );
  mesh.rotation.x += 0.01;
    mesh.rotation.y += 0.04;
  renderer.render(scene, camera);
};


